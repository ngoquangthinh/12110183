﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcApplication6.Models
{
    public class Tag
    {
        public int ID { set; get; }
        public String Body { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}