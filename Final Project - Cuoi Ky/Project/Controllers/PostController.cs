﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Project.Models;

namespace Project.Controllers
{
    [Authorize]
    public class PostController : Controller
    {
        private WebDbContext db = new WebDbContext();

        //
        // GET: /Post/
        [AllowAnonymous]
        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.TheLoai);
            return View(posts.ToList());
        }

        //
        // GET: /Post/Details/5
        [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            ViewData["idpost"] = id;
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTheLoai");
            return View();
        }

        //
        // POST: /Post/Create

        [HttpPost]
        public ActionResult Create(Post post, String content)
        {
            if (ModelState.IsValid)
            {
                post.DateCreated = DateTime.Now;

                //Tag t1 = new Tag();
                //t1.Content = content;
                //if (post.Tags == null)
                //    post.Tags = new List<Tag>();
                //post.Tags.Add(t1);
                //if (t1.Posts == null)
                //    t1.Posts = new List<Post>();
                //t1.Posts.Add(post);

                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName }).
                    Where(y => y.UserName == User.Identity.Name).Single().UserId;

                post.UserProfileID = userid;
                List<Tag> Tags = new List<Tag>();
                string[] tagContent = content.Split(',');
                foreach (string item in tagContent)
                {
                    Tag TagExist = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item.Trim()));
                    if (ListTag.Count() > 0)
                    {
                        TagExist = ListTag.First();
                        TagExist.Posts.Add(post);

                    }
                    else
                    {
                        TagExist = new Tag();
                        TagExist.Content = item.Trim();
                        TagExist.Posts = new List<Post>();
                        TagExist.Posts.Add(post);
                    }
                    Tags.Add(TagExist);
                }
                post.Tags = Tags;
                db.Posts.Add(post);
                //db.Tags.Add(t1);
                db.SaveChanges();
                //return RedirectToAction("Index");
                return RedirectToAction("Details/" + post.ID, "Post");
            }

            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTheLoai", post.TheLoaiID);
            return View(post);
            
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTheLoai", post.TheLoaiID);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.TheLoaiID = new SelectList(db.TheLoais, "ID", "TenTheLoai", post.TheLoaiID);
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}