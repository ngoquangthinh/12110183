﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required(ErrorMessage="Không Được Để Trống")]
        public string Body { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Không Được Để Trống")]
        public string Author { set; get; }

        public int PostID { set; get; }
    }
}