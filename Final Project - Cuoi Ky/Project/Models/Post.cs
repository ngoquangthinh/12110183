﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class Post
    {
        public int ID { set; get; }

        [Required(ErrorMessage="Không Được Bỏ Trống")]
        [StringLength (255, ErrorMessage="Số lượng ký tự từ 5 đến 255 ký tự", MinimumLength = 5)]
        public string Title { set; get; }

        [Required(ErrorMessage = "Không Được Bỏ Trống")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự từ 5 đến 500 ký tự", MinimumLength = 5)]
        public string Body { set; get; }
        
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileID { set; get; }

        public virtual TheLoai TheLoai { set; get; }
        public int TheLoaiID { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
        
    }
}