﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class Rate
    {
        [Key]
        [Column(Order=0)]
        public int UserProfileID { set; get; }
        public virtual UserProfile UserProfile { set; get; }
        [Key]
        [Column(Order = 1)]
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
        public int Diem { set; get; }
    }
}