﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Project.Models
{
    public class TheLoai
    {
        public int ID { set; get; }
        [Required(ErrorMessage="Không Được Để Trống")]
        public string TenTheLoai { set; get; }

        public virtual ICollection<Post> Posts { set; get; }

    }
}