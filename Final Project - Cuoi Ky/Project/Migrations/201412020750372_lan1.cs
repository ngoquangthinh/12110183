namespace Project.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false, maxLength: 255),
                        Body = c.String(nullable: false, maxLength: 500),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileID = c.Int(nullable: false),
                        TheLoaiID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .ForeignKey("dbo.TheLoais", t => t.TheLoaiID, cascadeDelete: true)
                .Index(t => t.UserProfile_UserId)
                .Index(t => t.TheLoaiID);
            
            CreateTable(
                "dbo.TheLoais",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TenTheLoai = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Body = c.String(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        PostID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Content = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Rates",
                c => new
                    {
                        UserProfileID = c.Int(nullable: false),
                        PostID = c.Int(nullable: false),
                        Diem = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => new { t.UserProfileID, t.PostID })
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .Index(t => t.UserProfile_UserId)
                .Index(t => t.PostID);
            
            CreateTable(
                "dbo.Tag_Post",
                c => new
                    {
                        PostID = c.Int(nullable: false),
                        TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PostID, t.TagID })
                .ForeignKey("dbo.Posts", t => t.PostID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.TagID, cascadeDelete: true)
                .Index(t => t.PostID)
                .Index(t => t.TagID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Tag_Post", new[] { "TagID" });
            DropIndex("dbo.Tag_Post", new[] { "PostID" });
            DropIndex("dbo.Rates", new[] { "PostID" });
            DropIndex("dbo.Rates", new[] { "UserProfile_UserId" });
            DropIndex("dbo.Comments", new[] { "PostID" });
            DropIndex("dbo.Posts", new[] { "TheLoaiID" });
            DropIndex("dbo.Posts", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.Tag_Post", "TagID", "dbo.Tags");
            DropForeignKey("dbo.Tag_Post", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Rates", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Rates", "UserProfile_UserId", "dbo.UserProfile");
            DropForeignKey("dbo.Comments", "PostID", "dbo.Posts");
            DropForeignKey("dbo.Posts", "TheLoaiID", "dbo.TheLoais");
            DropForeignKey("dbo.Posts", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.Tag_Post");
            DropTable("dbo.Rates");
            DropTable("dbo.Tags");
            DropTable("dbo.Comments");
            DropTable("dbo.TheLoais");
            DropTable("dbo.Posts");
            DropTable("dbo.UserProfile");
        }
    }
}
