﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication6.Models
{
    public class Post
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 5", MinimumLength = 5)]
        public string Title { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public string Body { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }

        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}