﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication6.Models
{
    public class Comment
    {
        public int ID { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public String Body { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }

        [Required(ErrorMessage="Không Được Để Trống")]
        public String Author { set; get; }

        public int PostID { set; get; }
    }
}