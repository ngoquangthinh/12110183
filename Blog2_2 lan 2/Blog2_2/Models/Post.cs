﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    [Table("Post")]
    public class Post
    {
        public int ID { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "So Luong Ky Tu Tu 20 den 500", MinimumLength = 20)]
        public String Title { set; get; }
        [StringLength(500,ErrorMessage="So Luong Ky Tu Toi Thieu 50 Ky Tu",MinimumLength=50)]
        public String Body { set; get; }
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }

        public virtual Account Account { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }

        public virtual ICollection<Tag> Tags { set; get; }
    }
}