﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "So Luong Ky Tu Tu 10 den 100", MinimumLength = 10)]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }

    }
}