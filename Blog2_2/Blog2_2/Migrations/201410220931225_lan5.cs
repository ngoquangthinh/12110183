namespace Blog2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(),
                        Email = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.AccountID);
            
            AddColumn("dbo.Post", "Account_AccountID", c => c.Int());
            AlterColumn("dbo.Post", "Title", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Post", "Body", c => c.String(maxLength: 500));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false, maxLength: 500));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
            AddForeignKey("dbo.Post", "Account_AccountID", "dbo.Accounts", "AccountID");
            CreateIndex("dbo.Post", "Account_AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Post", new[] { "Account_AccountID" });
            DropForeignKey("dbo.Post", "Account_AccountID", "dbo.Accounts");
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.Post", "Body", c => c.String(maxLength: 250));
            AlterColumn("dbo.Post", "Title", c => c.String(nullable: false));
            DropColumn("dbo.Post", "Account_AccountID");
            DropTable("dbo.Accounts");
        }
    }
}
