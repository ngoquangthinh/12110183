namespace Blog2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Posts", newName: "Post");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Post", newName: "Posts");
        }
    }
}
