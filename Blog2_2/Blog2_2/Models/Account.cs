﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Account
    {
        public int AccountID { set; get; }
        [Required]
        [StringLength(100, ErrorMessage = "So Luong Ky Tu Toi Da 100 Ky Tu", MinimumLength = 1)]
        public String FirstName { set; get; }
        public String LastName { set; get; }
        [DataType(DataType.EmailAddress)]
        public String Email { set; get; }
        [DataType(DataType.Password)]
        public String Password { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}