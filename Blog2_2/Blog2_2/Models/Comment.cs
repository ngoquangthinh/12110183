﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Comment
    {
        public int ID { set; get; }
        //public String Title { set; get; }
        [Required]
        [StringLength(500, ErrorMessage = "So Luong Ky Tu Tu 50 den 500", MinimumLength = 50)]
        public String Body { set; get; }
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }
        public String Author { set; get; }

        public String PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}