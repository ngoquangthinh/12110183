﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication5.Models
{
    public class Tag
    {
        public int ID { set; get; }
        //
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(100, ErrorMessage = "Số lượng ký tự từ 10-100 ký tự", MinimumLength = 10)]
        public String Content { set; get; }
        //1 Tag co nhieu Post
        public virtual ICollection<Post> Posts { set; get; }
    }
}