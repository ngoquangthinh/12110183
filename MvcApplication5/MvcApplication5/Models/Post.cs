﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MvcApplication5.Models
{
    public class Post
    {
        public int ID { set; get; }
        //
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự từ 20-500", MinimumLength = 2)]
        public String Title { set; get; }
        //
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự tối thiểu là 50", MinimumLength = 5)]
        public String Body { set; get; }
        //
        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.Date)]
        public DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Dữ liệu chưa được nhập")]
        [DataType(DataType.Date)]
        public DateTime DateUpdated { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}